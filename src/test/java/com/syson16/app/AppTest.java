package com.syson16.app;

import static org.junit.Assert.assertEquals;
import java.util.List;

import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
    /**
     * Rigorous Test :-)
     */
    @Test
    public void testSearchInput1()
    {
        String fileName = "target/test-classes/input1";
        Parser parser = new Parser();
        Game game = parser.parse(fileName);
        List<String> answer = game.getAnswer();
        assertEquals(answer.size(), 2);
        String[] expected = new String[] {
            "ABC 0:0 0:2",
            "AEI 0:0 2:2"
        };
        for (int i = 0; i < expected.length; i++) {
            assertEquals(expected[i], answer.get(i));
        }
    }

    @Test
    public void testSearchInput2()
    {
        String fileName = "target/test-classes/input2";
        Parser parser = new Parser();
        Game game = parser.parse(fileName);
        List<String> answer = game.getAnswer();
        assertEquals(answer.size(), 3);
        String[] expected = new String[] {
            "HELLO 0:0 4:4",
            "GOOD 4:0 4:3",
            "BYE 1:3 1:1"
        };
        for (int i = 0; i < expected.length; i++) {
            assertEquals(expected[i], answer.get(i));
        }
    }

    @Test
    public void testSearchInput3()
    {
        String fileName = "target/test-classes/input3";
        Parser parser = new Parser();
        Game game = parser.parse(fileName);
        List<String> answer = game.getAnswer();
        assertEquals(answer.size(), 5);
        String[] expected = new String[] {
            "HELLO 0:0 4:4",
            "GOOD 4:0 4:3",
            "BYE 1:3 1:1",
            "HAS 0:0 0:2",
            "GEY 1:0 1:2"
        };
        for (int i = 0; i < expected.length; i++) {
            assertEquals(expected[i], answer.get(i));
        }
    }

}
