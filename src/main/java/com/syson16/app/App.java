package com.syson16.app;

import java.util.List;

/**
 * Alphabet Soup
 * Sangyeon Son
 */
public class App 
{
    public static void main( String[] args )
    {
        if(args.length > 0) {

            String fileName = args[0];
            
            Parser parser = new Parser();

            // Parse data
            Game game = parser.parse(fileName);

            // Get the answer
            List<String> answer = game.getAnswer();
            for (int i = 0; i < answer.size(); i++) {
                System.out.println(answer.get(i));
            }
        } else {
            System.out.println("Please pass the file path");
        }
    }
}
