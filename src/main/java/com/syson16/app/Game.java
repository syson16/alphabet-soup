package com.syson16.app;

import java.util.ArrayList;
import java.util.List;

public class Game {

    private List<Integer> gridSize;

    private List<String> grid;

    private List<String> words;

    public Game() { }

    public List<Integer> getGridSize() {
        return gridSize;
    }
    public void setGridSize(List<Integer> gridSize) {
        this.gridSize = gridSize;
    }
    public List<String> getGrid() {
        return grid;
    }
    public void setGrid(List<String> grid) {
        this.grid = grid;
    }
    public List<String> getWords() {
        return words;
    }
    public void setWords(List<String> words) {
        this.words = words;
    }
    public List<String> getAnswer(){
        List<String> answer = new ArrayList<>();

        int rowSize = this.gridSize.get(0);
        int colSize = this.gridSize.get(1);

		char[][] gridBox = new char[rowSize][colSize];

        // Fill the grid
		for (int x = 0; x < rowSize; x++) {
            String[] fillGrid = this.grid.get(x).split(" ");
            
            for (int y = 0; y < colSize; y++) {
                gridBox[x][y] = fillGrid[y].charAt(0);
            }
        }

        // loop through the words
        for (String currentWord : this.words ) {
            for (int r = 0; r < rowSize; r++) {
                for (int c = 0; c < colSize; c++) {	
                    // first char doesn't match
                    if(gridBox[r][c] != currentWord.charAt(0)) {
                        continue; 
                    }
                    
                    // 8 directions clockwise:

                    // grid example
                    // [0][0] [0][1] [0][2] [0][3] [0][4] ...
                    // [1][0] [1][1] [1][2] [1][3] [1][4] ...
                    // [2][0] [2][1] [2][2] [2][3] [2][4] ...
                    // [3][0] [3][1] [3][2] [3][3] [3][4] ...
                    // [4][0] [4][1] [4][2] [4][3] [4][4] ...

                    // Top
                    if (r - currentWord.length() >= -1) {  //
                        for (int i = 0; i < currentWord.length(); i++) {
                            if (gridBox[r - i][c] == currentWord.charAt(i)) {
                                if (i == currentWord.length() - 1)
                                {
                                    answer.add(currentWord + " " + r + ":" + c + " " + (r - currentWord.length() + 1) + ":" + c);
                                }
                            }
                            else {
                                break;
                            }
                        }
                    }

                    // Top right
                    if (r >= currentWord.length() - 1 && c + currentWord.length() <= colSize) {
                        for (int i = 0; i < currentWord.length(); i++) {
                            if (gridBox[r - i][c + i] == currentWord.charAt(i)) {
                                if (i == currentWord.length() - 1) {
                                    answer.add(currentWord + " " + r + ":" + c + " " + (r - currentWord.length() + 1) + ":" + (c + currentWord.length() - 1) );
                                }
                            }
                            else {
                                break;
                            }
                        }
                    }

                    // Right
                    if (c + currentWord.length() <= colSize) {
                        for (int i = 0; i < currentWord.length(); i++) {
                            if (gridBox[r][c + i] == currentWord.charAt(i)) {
                                if (i == currentWord.length() - 1)
                                {
                                    answer.add(currentWord + " " + r + ":" + c + " " + r + ":" + (c + currentWord.length() - 1));
                                }
                            }
                            else {
                                break;
                            }
                        }
                    }

                    // Right bottom
                    if (r + currentWord.length() <= rowSize && c + currentWord.length() <= colSize) {
                        for (int i = 0; i < currentWord.length(); i++) {
                            if (gridBox[r + i][c + i] == currentWord.charAt(i)) {
                                if (i == currentWord.length() - 1)
                                {
                                    answer.add(currentWord + " " + r + ":" + c + " " + (r + currentWord.length() - 1) + ":" + (c + currentWord.length() - 1) );
                                }
                            }
                            else {
                                break;
                            }
                        }
                    }

                    // Bottom
                    if (r + currentWord.length() <= rowSize) {
                        for (int i = 0; i < currentWord.length(); i++) {
                            if (gridBox[r + i][c] == currentWord.charAt(i)) {
                                if (i == currentWord.length() - 1)
                                {
                                    answer.add(currentWord + " " + r + ":" + c + " " + (r + currentWord.length() - 1) + ":" + c);
                                }
                            }
                            else {
                                break;
                            }
                        }
                    }

                    // Left bottom
                    if (r + currentWord.length() <= rowSize && c - currentWord.length() >= -1) {
                        for (int i = 0; i < currentWord.length(); i++) {
                            if (gridBox[r + i][c - i] == currentWord.charAt(i)) {
                                if (i == currentWord.length() - 1) {
                                    answer.add(currentWord + " " + r + ":" + c + " " + (r + currentWord.length() - 1) + ":" + (c - currentWord.length() + 1));
                                }
                            }
                            else {
                                break;
                            }
                        }
                    } 
                    
                    // Left
                    if (c - currentWord.length()  >= -1) {	//gl
                        for (int i = 0; i < currentWord.length(); i++) {
                            if (gridBox[r][c - i] == currentWord.charAt(i)) {
                                if (i == currentWord.length() - 1)
                                {
                                    answer.add(currentWord + " " + r + ":" + c + " " + r + ":" + (c - currentWord.length() + 1));
                                }
                            }
                            else {
                                break;
                            }
                        }
                    }
                    
                    // Left Top
                    if (r >= currentWord.length() - 1 && c >= currentWord.length() - 1) {
                        for (int i = 0; i < currentWord.length(); i++) {
                            if (gridBox[r - i][c - i] == currentWord.charAt(i)) {
                                if (i == currentWord.length() - 1) {
                                    answer.add(currentWord + " " + r + ":" + c + " " + (r - currentWord.length() + 1) + ":" + (c - currentWord.length() + 1) );
                                }
                            }
                            else {
                                break;
                            }
                        }
                    }
                }
            }
        }

        return answer;
    }

}