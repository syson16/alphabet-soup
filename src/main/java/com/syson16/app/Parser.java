package com.syson16.app;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.lang.Integer;
import java.util.ArrayList;
import java.util.List;

public class Parser {

    public Parser() { }

    // Parse the data

    public Game parse(String fileName) {
        
        // Read file
        List<String> lines = new ArrayList<>();
        BufferedReader br = null;
        
        try {
            br = new BufferedReader(new FileReader(fileName));

            String oneline;
            while ((oneline = br.readLine()) != null) {
                lines.add(oneline);
            }
            
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        
        // Get grid size
        String[] gridSize = lines.get(0).split("x");
        
        // Get the column size of the grid
        int rowSize = Integer.parseInt(gridSize[0]);
        int colSize = Integer.parseInt(gridSize[1]);
        List<Integer> gridSizeInt = new ArrayList<>();
        gridSizeInt.add(rowSize);
        gridSizeInt.add(colSize);

        // Get grid
        List<String> grid = new ArrayList<>();
        for (int i = 1; i < colSize + 1; i++) {
            grid.add(lines.get(i));
        }

        // Get words
        List<String> words = new ArrayList<>();
        for (int i = 1 + colSize; i < lines.size(); i++) {
            words.add(lines.get(i));
        }

        Game game = new Game();
        game.setGridSize(gridSizeInt);
        game.setGrid(grid);
        game.setWords(words);

        return game;
    }
}